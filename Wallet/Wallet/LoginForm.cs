﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Wallet.Class;

namespace Wallet
{
    public partial class LoginForm : Form
    {
        bool userOk = false;
        public LoginForm()
        {
            InitializeComponent();   
                     
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (userOk)
            {
                User.CreatUser(txtFName.Text, txtLName.Text, txtUserName.Text, txtPassword.Text);
                MessageBox.Show("Registration completed successfully");
                txtFName.Text = "";
                txtLName.Text = "";
                txtUserName.Text = "";
                txtPassword.Text = "";
            }
            else
            {
                MessageBox.Show("username not valid");
            }
            txtUserName_TextChanged(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            if (User.Exist(txtLogin.Text))
            {
                string path = "data\\" + txtLogin.Text + ".dat";
                FileStream file = new FileStream(path, FileMode.Open);
                StreamReader dataIn = new StreamReader(file);
                string str = dataIn.ReadToEnd();
                string[] arr;
                string userPass = "";
                arr = str.Split('\n');
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i].Length >= 8 && arr[i].Substring(0, 8) == "Password")
                    {
                        userPass = arr[i].Substring(9);
                    }
                }
                if (userPass.Substring(0,userPass.Length-1) == txtPass.Text)
                {
                    file.Close();
                    WalletForm walletForm = new WalletForm(txtLogin.Text);
                    walletForm.Show();
                    Hide();
                    
                }
                //MessageBox.Show(userPass);
                //file.Close();
            }
            else
            {
                MessageBox.Show("UserName or Password is wrong");
            }
        }

        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            if ((txtUserName.Text == "") || (User.Exist(txtUserName.Text)))
            {
                imageTrueFalse.BackgroundImage = imageList.Images[0];
                userOk = false;
            }
            else
            {
                imageTrueFalse.BackgroundImage = imageList.Images[1];
                userOk = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtUserName_TextChanged(sender, e);
        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
