﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Wallet.Class
{
    class User
    {
        //string _firstName;
        //string _lastName;
        //string _userName;
        //string _password;
        //decimal _balance;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public decimal Balance { get; set; }
        public decimal Income { get; set; }
        public decimal Expense { get; set; }

        public string[] expense = new string[] { "Food & Drink", "Shopping", "Rents", "Education", "Internet", "Phone" };
        public string[] income = new string[] { "Salary", "Credit", "Other Income" };
        public User()
        {
            FirstName = "";
            LastName = "";
            UserName = "";
            Password = "";
        }
        public User(string userName)
        {
            UserName = userName;
            if (User.Exist(userName))
            {
                string path = "data\\" + userName + ".dat";
                FileStream file = new FileStream(path, FileMode.Open);
                StreamReader dataIn = new StreamReader(file);
                string str = dataIn.ReadToEnd();
                string[] arr;
                arr = str.Split('\n');
                for (int i = 0; i < 6; i++)
                {
                    string[] strArr = arr[i].Split(':');
                    switch (strArr[0])
                    {
                        case "First Name":
                            FirstName = strArr[1];
                            break;
                        case "Last Name":
                            LastName = strArr[1];
                            break;
                        case "Balance":
                            Balance = Convert.ToDecimal(strArr[1]); 
                            break;
                        case "Income":
                            Income = Convert.ToDecimal(strArr[1]);
                            break;
                        case "Expense":
                            Expense = Convert.ToDecimal(strArr[1]);
                            break;
                        default:
                            break;
                    }
                }
                file.Close();
            }
        }
        public User(string firstName, string lastName, string userName, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            UserName = userName;
            Password = password;
        } 

        public static void CreatUser(string firstName, string lastName, string userName, string password)
        {
            
            FileStream file;
            string path;
            path = "data\\" + userName + ".dat";
            file = new FileStream(path, FileMode.Create);
            file.Close();
            StreamWriter writeInfo = new StreamWriter(path);
            writeInfo.WriteLine("First Name:{0}", firstName);
            writeInfo.WriteLine("Last Name:{0}", lastName);
            writeInfo.WriteLine("Password:{0}", password);
            writeInfo.WriteLine("Balance:0");
            writeInfo.WriteLine("Income:0");
            writeInfo.WriteLine("Expense:0");
            writeInfo.Close();
        }
        public void AddRecord(int InOrOut,decimal amount, int category, string note)
        {
            string path;
            path = "data\\" + UserName + ".dat";
            StreamReader dataIn = new StreamReader(path);
            string str = dataIn.ReadToEnd();
            dataIn.Close();
            string[] arr;
            arr = str.Split('\n');
            string[] strArr;
            if (InOrOut == 0)
            {
                strArr = arr[3].Split(':');
                arr[3] = String.Format("{0}:{1}", strArr[0], Convert.ToDecimal(strArr[1]) - amount);
                strArr = arr[5].Split(':');
                arr[5] = String.Format("{0}:{1}", strArr[0], Convert.ToDecimal(strArr[1]) + amount);
            }
            else
            {
                strArr = arr[3].Split(':');
                arr[3] = String.Format("{0}:{1}", strArr[0], Convert.ToDecimal(strArr[1]) + amount);
                strArr = arr[4].Split(':');
                arr[4] = String.Format("{0}:{1}", strArr[0], Convert.ToDecimal(strArr[1]) + amount);
            }
            str = String.Join("\n", arr);
            StreamWriter writeRecord = new StreamWriter(path);
            writeRecord.Write(str);
            writeRecord.WriteLine("{0}|{1}|{2}|{3}|{4}", InOrOut, amount, category, note, DateTime.Now);
            writeRecord.Close();
        }

        public string GetLastRecords()
        {
            string path;
            path = "data\\" + UserName + ".dat";
            StreamReader dataIn = new StreamReader(path);
            string str = dataIn.ReadToEnd();
            dataIn.Close();
            string[] arr;
            arr = str.Split('\n');
            string newString = "";
            for (int i = arr.Length - 2; i > 5; i--)
            {
                string tempString = "";
                string[] strArr = arr[i].Split('|');
                switch (strArr[0])
                {
                    case "0":
                        tempString = String.Format("Expense:\t{0,8:C}\t{1,10}\t{2}\t{3}", Convert.ToDecimal(strArr[1]), expense[Convert.ToInt16(strArr[2])],strArr[3],strArr[4]);
                        break;
                    case "1":
                        tempString = String.Format("Income:\t{0,8:C}\t{1,10}\t{2}\t{3}", Convert.ToDecimal(strArr[1]), income[Convert.ToInt16(strArr[2])], strArr[3], strArr[4]);                 
                        break;
                    default:
                        break;
                }
                newString += tempString + '\n';
            }
            return newString;
        }

        public static bool Exist(string userName)
        {
            //FileStream file;
            string path = "data\\" + userName + ".dat";
            return File.Exists(path) ;
        }

    }
}
