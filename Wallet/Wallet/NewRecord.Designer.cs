﻿namespace Wallet
{
    partial class NewRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtINote = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtICategory = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIBalance = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtENote = new System.Windows.Forms.TextBox();
            this.txtEAmount = new System.Windows.Forms.TextBox();
            this.txtEBalance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtECategory = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.ItemSize = new System.Drawing.Size(200, 25);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(60, 3);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(353, 300);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtINote);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtICategory);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtIAmount);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtIBalance);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(345, 267);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Income";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtINote
            // 
            this.txtINote.Location = new System.Drawing.Point(148, 129);
            this.txtINote.Margin = new System.Windows.Forms.Padding(4);
            this.txtINote.Multiline = true;
            this.txtINote.Name = "txtINote";
            this.txtINote.Size = new System.Drawing.Size(148, 46);
            this.txtINote.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(97, 129);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 18);
            this.label1.TabIndex = 39;
            this.label1.Text = "Note:";
            // 
            // txtICategory
            // 
            this.txtICategory.FormattingEnabled = true;
            this.txtICategory.Location = new System.Drawing.Point(148, 93);
            this.txtICategory.Name = "txtICategory";
            this.txtICategory.Size = new System.Drawing.Size(148, 26);
            this.txtICategory.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 37;
            this.label2.Text = "Category:";
            // 
            // txtIAmount
            // 
            this.txtIAmount.Location = new System.Drawing.Point(148, 47);
            this.txtIAmount.Margin = new System.Windows.Forms.Padding(4);
            this.txtIAmount.Name = "txtIAmount";
            this.txtIAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtIAmount.Size = new System.Drawing.Size(148, 24);
            this.txtIAmount.TabIndex = 36;
            this.txtIAmount.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 18);
            this.label3.TabIndex = 35;
            this.label3.Text = "Amount:";
            // 
            // txtIBalance
            // 
            this.txtIBalance.Location = new System.Drawing.Point(148, 8);
            this.txtIBalance.Margin = new System.Windows.Forms.Padding(4);
            this.txtIBalance.Name = "txtIBalance";
            this.txtIBalance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtIBalance.Size = new System.Drawing.Size(148, 24);
            this.txtIBalance.TabIndex = 34;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 33;
            this.label4.Text = "Balance:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(98, 183);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 32);
            this.button1.TabIndex = 32;
            this.button1.Text = "Add record";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtENote);
            this.tabPage2.Controls.Add(this.txtEAmount);
            this.tabPage2.Controls.Add(this.txtEBalance);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtECategory);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(345, 267);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Expense";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtENote
            // 
            this.txtENote.Location = new System.Drawing.Point(147, 129);
            this.txtENote.Margin = new System.Windows.Forms.Padding(4);
            this.txtENote.Multiline = true;
            this.txtENote.Name = "txtENote";
            this.txtENote.Size = new System.Drawing.Size(148, 46);
            this.txtENote.TabIndex = 40;
            // 
            // txtEAmount
            // 
            this.txtEAmount.Location = new System.Drawing.Point(147, 47);
            this.txtEAmount.Margin = new System.Windows.Forms.Padding(4);
            this.txtEAmount.Name = "txtEAmount";
            this.txtEAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtEAmount.Size = new System.Drawing.Size(148, 24);
            this.txtEAmount.TabIndex = 36;
            this.txtEAmount.Text = "0";
            // 
            // txtEBalance
            // 
            this.txtEBalance.Location = new System.Drawing.Point(147, 8);
            this.txtEBalance.Margin = new System.Windows.Forms.Padding(4);
            this.txtEBalance.Name = "txtEBalance";
            this.txtEBalance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtEBalance.Size = new System.Drawing.Size(148, 24);
            this.txtEBalance.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(96, 129);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 18);
            this.label8.TabIndex = 39;
            this.label8.Text = "Note:";
            // 
            // txtECategory
            // 
            this.txtECategory.FormattingEnabled = true;
            this.txtECategory.Location = new System.Drawing.Point(147, 93);
            this.txtECategory.Name = "txtECategory";
            this.txtECategory.Size = new System.Drawing.Size(148, 26);
            this.txtECategory.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(68, 96);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 18);
            this.label6.TabIndex = 37;
            this.label6.Text = "Category:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(77, 50);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 18);
            this.label5.TabIndex = 35;
            this.label5.Text = "Amount:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(75, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 18);
            this.label7.TabIndex = 33;
            this.label7.Text = "Balance:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(97, 183);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 32);
            this.button2.TabIndex = 32;
            this.button2.Text = "Add record";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // NewRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 281);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewRecord";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "NewRecord";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtINote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox txtICategory;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIBalance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtENote;
        private System.Windows.Forms.TextBox txtEAmount;
        private System.Windows.Forms.TextBox txtEBalance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox txtECategory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button2;
    }
}