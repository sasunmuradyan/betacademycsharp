﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Wallet.Class;

namespace Wallet
{
    public partial class WalletForm : Form
    {
        //public event newRecordD newRecordClose;
        string _userName;
        public WalletForm()
        {
            InitializeComponent();
        }
        public WalletForm(string userName)
        {
            InitializeComponent();
            _userName = userName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NewRecord newRecord = new NewRecord(_userName);
            newRecord.ShowDialog();
            Close();
            
        }
        public void DataUpdate(string userName)
        {
            User user = new User(userName);
            lblFullName.Text = String.Format("User: {0} {1}", user.FirstName.Trim(), user.LastName);
            txtBalance.Text = String.Format("{0:C}",user.Balance);
            txtIncome.Text = String.Format("{0:C}",user.Income);
            txtExpense.Text = String.Format("-{0:C}",user.Expense);
            txtLastRecords.Text = user.GetLastRecords();           
        }

        private void WalletForm_Load(object sender, EventArgs e)
        {
            DataUpdate(_userName);
        }
    }
}
