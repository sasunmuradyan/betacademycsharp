﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkString
{
    class Program
    {
        static void FindAllSubstring(string str)
        {
            int count = 0;
            int n = str.Length;
            int m = 1;
            count = (n + 1) / 2 * n;
            Console.WriteLine("Count substrings of \"{0}\" is equal {1}.",str,count);
            while (m <= n)
            {
                for (int i = 0; i <= n - m; i++)
                {
                    Console.WriteLine(str.Substring(i,m));
                }
                m++;
            }        
        }
        static void FindSubstring(string str, string sbstr)
        {
            int n = str.Length;
            int ind = -1;
            Console.WriteLine("start indexes \"{0}\" of \"{1}\"", sbstr,str);
            while (true)
            {
                ind = str.IndexOf(sbstr, ind + 1);
                if (ind == -1)
                    break;
                Console.WriteLine(ind);            
            }
        }
        static void Main(string[] args)
        {
            string str = "abrakadabra abrab";
            string sbstr = "ab";
            FindAllSubstring(str);
            Console.WriteLine();
            FindSubstring(str, sbstr);
            Console.Read();
        }
    }
}
