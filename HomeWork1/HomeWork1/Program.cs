﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int[7] { 1, 5, 6, 2, 7, 13, 1 }; //ind=4
            int[] arr2 = new int[12] { 1, 20, 1, 0, 2, 0, 3, 5, 6, 7, 14, 0 }; //ind=7
            int[] arr3 = new int[3] { 8, 12, 16 }; //count=3

            Console.WriteLine(ind(arr1));
            Console.WriteLine(ind(arr2));
            Console.WriteLine(count(arr3));
            Console.ReadKey();
        }
        static int ind(int[] x)
        {
            int leftSum = 0;
            int leftCount = 0;
            int rightSum = 0;
            int rightCount = 0;
            int n = x.Length;
            int ind = -1;
            if (n < 3)
            {
                return ind;
            }
            else
            {
                leftSum = x[0];
                rightSum = x[n - 1];
                leftCount = 1;
                rightCount = 1;
                int leftInd = 1;
                int rightInd = n - 2;
                ind = 1;
                do
                {
                    if (leftCount + rightCount == n - 1)
                    {
                        if (leftSum == rightSum)
                        {
                            return ind;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        if (leftSum > rightSum)
                        {
                            rightSum += x[rightInd];
                            rightInd--;
                            rightCount++;
                        }
                        else
                        {
                            leftSum += x[leftInd];
                            leftInd++;
                            leftCount++;
                            ind++;
                        }
                    }

                } while (leftCount + rightCount <= n - 1);
            }
            return ind;   
        }
        static int count(int[] x)
        {
            int count = 0;
            int min = arrMin(x);
            bool t;
            for (int k = 1; k <= min; k++)
            {
                t = true;
                for (int i = 0; i < x.Length; i++)
                {
                    if (x[i] % k != 0)
                    {
                        t = false;
                        //break;
                    }
                }
                if (t)
                {
                    count++;
                }
            }
            return count;
        }
        static int arrMin(int[] x)
        {
            int min = x[0];
            for (int i = 1; i < x.Length; i++)
            {
                min = Math.Min(min, x[i]);
            }
            return min;
        }
    }
}
