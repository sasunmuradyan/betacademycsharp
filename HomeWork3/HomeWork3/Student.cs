﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    class Student:Person
    {
        private int _course;
        private string _group;
        public Student(string name, string surname, int age, string address, int course, string group):base(name, surname, age, address)
        {
            this._course = course;
            this._group = group;
        }
        public void Display()
        {
            Console.WriteLine("Student info");
            Console.WriteLine("Name: {0} {1}", name, surname);
            Console.WriteLine("age: {0}", age);
            Console.WriteLine("address {0}", address);
            Console.WriteLine("course: {0}", _course);
            Console.WriteLine("group: {0}", _group);
            
        }
    }
}
