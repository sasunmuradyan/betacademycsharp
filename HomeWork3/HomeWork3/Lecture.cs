﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    class Lecture:Person
    {
        private string _ambionName;

        public Lecture(string name, string surname, int age, string address, string ambionName):base(name, surname, age, address)
        {
            this._ambionName = ambionName;
        }
    }
}
