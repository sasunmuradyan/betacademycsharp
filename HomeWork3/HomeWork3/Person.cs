﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork3
{
    class Person
    {
        protected string name;
        protected string surname;
        protected int age;
        protected string address;
        public Person(string name, string surname, int age, string address)
        {
            this.name = name;
            this.surname = surname;
            this.age = age;
            this.address = address;
        }

    }
}
