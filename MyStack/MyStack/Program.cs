﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    class Program
    {
        static void Main(string[] args)
        {
            MyStack<int> stack = new MyStack<int>(50);
            stack.Push(5);
            stack.Push(4);
            stack.Push(3);
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Top());

        }
    }
}
