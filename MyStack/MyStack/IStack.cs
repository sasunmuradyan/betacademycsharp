﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    interface IStack<Type>
    {
        void Push(Type data);
        Type Pop();
        Type Top();
    }
}
