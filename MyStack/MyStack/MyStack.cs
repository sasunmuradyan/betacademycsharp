﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    class MyStack<Type>:IStack<Type>
    {
        int _currentIndex = -1;
        Type[] arr;
        public MyStack(int n)
        {
            arr = new Type[n];
        }
        public void Push(Type d)
        {
            arr[++_currentIndex] = d;
        }
        public Type Pop()
        {
            if (_currentIndex < 0)
            {
                throw new Exception("stack is empty");
            }
            return arr[_currentIndex--];
        }
        public Type Top()
        {
            if (_currentIndex < 0)
            {
                throw new Exception("stack is empty");
            }
            return arr[_currentIndex];
        }
        public void PushBottom(Type data)
        {
            if (_currentIndex == 0)
            {
                Type temp = Pop();
                Push(data);
                Push(temp);
            }
            else
            {
                Type temp = Pop();
                PushBottom(data);
                Push(temp);
            }

        }

        public override string ToString()
        {
            string temp = "";
            for (int i = 0; i < 5; i++)
            {
                temp += arr[i] + " ";
            }
            return temp;
        }
    }
}
