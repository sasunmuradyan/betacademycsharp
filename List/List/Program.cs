﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using List.Class;

namespace List
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList list = new LinkedList();
            list.Add(1);
            list.Add(3);
            list.Add(6);
            list.Add(0);
            list.Add(1);
            list.AddBegining(6);
            //Console.WriteLine(list.ToString());
            //Console.WriteLine("count of deleted nodes equal {0}", list.Remove(6)); 
            Console.WriteLine(list.ToString());
            list.RemoveDuplicates();
            Console.WriteLine(list.ToString());
            
            //list.PrintAllNodes();
            Console.ReadKey();

        }
    }
}
