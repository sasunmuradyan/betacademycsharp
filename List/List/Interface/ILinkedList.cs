﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using List.Class;
namespace List.Interface
{
    interface ILinkedList
    {
        int Length { get; set; }
        void Add(int d);
        void AddBegining(int d);
        void AddAfter(Node n, int d);
        int Remove(int d);
        void RemoveAfter(Node n);
        void PrintAllNodes();
        void RemoveDuplicates();

    }
}
