﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using List.Interface;


namespace List.Class
{
    class LinkedList:ILinkedList
    {
        Node head;
        Node last;
        public int Length {get; set;}
        public LinkedList()
        {
            head = null;
            Length = 0;
            last = null;
        }
        public void Add(int d)
        {
            Node newNode = new Node(d);
            if (head == null)
            {
                head = newNode;
                last = newNode;      
                Length = 1;
            }
            else
            {
                last.next = newNode;
                last = newNode;
                Length++;
            }
        }
        public void AddBegining(int d)
        {
            Node newNode = new Node(d);
            if(head == null)
            {
                head = newNode;
                last = newNode;
                Length = 1;
            }
            else
            {
                newNode.next = head;
                head = newNode;
                Length++;
            }
        }
        public void AddAfter(Node n, int d)
        {
            Node newNode = new Node(d);
            newNode.next = n.next;
            n.next = newNode;
            Length++;
        }
        public int Remove(int d)
        {
            int count = 0;
            Node temp;
            Node prevTemp;
            temp = head;
            prevTemp = null;
            while (temp != null)
            {
                if (temp.data == d)
                {
                    if (prevTemp != null)
                    {
                        prevTemp.next = temp.next;
                    }
                    else
                    {
                        head = temp.next;
                    }
                    count++;
                }
                prevTemp = temp;
                temp = temp.next;
            }
            Length -= count;
            return count; //count of deleted nodes
        }
        public void RemoveAfter(Node n)
        {
            n.next = n.next.next;
            Length--;
        }
        public void PrintAllNodes()
        {
            Node temp;
            temp = head;
            while(temp != null)
            {
                Console.WriteLine(temp.data);
                temp = temp.next;
            }
           
        }

        public override string ToString()
        {
            string str = String.Format("List Length = {0} \n",Length);
            Node temp;
            temp = head;
            while (temp != null)
            {
                str += String.Format("{0} ",temp.data);
                temp = temp.next;
            }
            return str;
        }

        public void RemoveDuplicates()
        {          
            Dictionary<int, int> dict = new Dictionary<int, int>();
            Node temp;
            Node prevTemp;
            temp = head;
            prevTemp = null;
            while (temp != null)
            {
                if (dict.ContainsKey(temp.data))
                {
                    RemoveAfter(prevTemp);
                    dict[temp.data]++;
                }
                else
                {
                    dict.Add(temp.data, 1);
                }
                prevTemp = temp;
                temp = temp.next;
            }          
        }
    }
}
