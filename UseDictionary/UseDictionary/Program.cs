﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[7] { 1, 2, 4, 5, 2, 1, 5 }; //4
            Dictionary<int, int> dict = new Dictionary<int, int>();
            for (int i = 0; i < arr.Length; i++)
            {
                if(dict.ContainsKey(arr[i]))
                {
                    dict[arr[i]]++;
                }
                else
                {
                    dict.Add(arr[i], 1);
                }
                
            }
            
            for (int i = 0; i < arr.Length; i++)
            {
                if (dict[arr[i]] == 1)
                {
                    Console.WriteLine(arr[i]);
                    break;
                }
            }
        }
        
    }
}
