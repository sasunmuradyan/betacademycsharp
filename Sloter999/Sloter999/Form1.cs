﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sloter999
{
    public partial class Form1 : Form
    {
        GameManager game = new GameManager(100, 5, 5);
        public Form1()
        {
            InitializeComponent();
            
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            radioButton1.BackColor = radioButton1.Checked ? Color.Red: Color.Empty;
            game.changeBet(5);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            radioButton2.BackColor = radioButton2.Checked ? Color.Red : Color.Empty;
            game.changeBet(10);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            radioButton3.BackColor = radioButton3.Checked ? Color.Red : Color.Empty;
            game.changeBet(15);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (game.getBalance() < game.getBet())
            {
                MessageBox.Show("Insufficient balance");
            }
            else if (game.getSpinLimit() <= 0)
            {
                MessageBox.Show("Your limit has expired");
            }
            else
            {

                radioButton1.Enabled = false;
                radioButton2.Enabled = false;
                radioButton3.Enabled = false;
                timer1.Enabled = true;
                timer2.Enabled = true;
                timer3.Enabled = true;

                timer4.Enabled = true;
                //picBox1.BackgroundImage = imageList1.Images[1];
                //game.display();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            game.spin(picBox1, imageList1, 1);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            game.spin(picBox2, imageList1, 2);
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            game.spin(picBox3, imageList1, 3);
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            
            if (!timer2.Enabled)
            {
                timer3.Enabled = false;
            }
            if (!timer1.Enabled)
            {
                timer2.Enabled = false;
            }
            timer1.Enabled = false;
            if (!timer3.Enabled)
            {
                timer4.Enabled = false;
                game.checkWin();
                dataUpdate();
                radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton3.Enabled = true;
                //game.display();

            }
        }
        public void dataUpdate()
        {
            textBox1.Text = game.getBalance().ToString();
            textBox2.Text = game.getSpinLimit().ToString();
            textBox3.Text = game.getLastWin().ToString();

        }
    }
}
