﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sloter999
{
    class GameManager
    {
        private int _balance;
        private int _spinLimit;
        private int _bet;
        private int _lastWin;
        private int _num1;
        private int _num2;
        private int _num3;
        public GameManager()
        {
            _balance = 100;
            _spinLimit = 5;
            _bet = 5;
            _lastWin = 0;
        }
        public GameManager(int balance, int spinLimit, int bet)
        {
            _balance = balance;
            _spinLimit = spinLimit;
            _bet = bet;
            _lastWin = 0;
        }

        public void changeBet(int bet)
        {
            _bet = bet;
        }

        public int getBalance()
        {
            return _balance;
        }
        public int getSpinLimit()
        {
            return _spinLimit;
        }
        public int getLastWin()
        {
            return _lastWin;
        }
        public int getBet()
        {
            return _bet;
        }
        public void startGame()
        {
            

        }
        
        public void display()
        {
            MessageBox.Show(String.Format("Balance: {0} spinLimit: {1} bet: {2} num1: {3} num2: {4} num3: {5}", _balance.ToString(), _spinLimit.ToString(), _bet.ToString(), _num1.ToString(), _num2.ToString(), _num3.ToString() ));
        }

        public void spin(PictureBox picBox,ImageList imgList, byte i)
        {          
            Random random = new Random();
            int ind;
            ind = (int)(random.NextDouble() * 9 + 1);
            picBox.BackgroundImage = imgList.Images[ind];
            switch (i)
            {
                case 1:
                    _num1 = ind;
                    break;
                case 2:
                    _num2 = ind;
                    break;
                case 3:
                    _num3 = ind;
                    break;
                default:
                    break;
            }
        }

        public int multiplier()
        {
            if ((_num1 == 9) && (_num2 == 9) && (_num3 == 9))
            {
                return 10;
            }
            else if ((_num1 == 8) && (_num2 == 8) && (_num3 == 8))
            {
                return 9;
            }
            else if ((_num1 == 7) && (_num2 == 7) && (_num3 == 7))
            {
                return 8;
            }
            else if ((_num1 == 6) && (_num2 == 6) && (_num3 == 6))
            {
                return 7;
            }
            else if ((_num1 == 5) && (_num2 == 5) && (_num3 == 5))
            {
                return 6;
            }
            else if ((_num1 == 4) && (_num2 == 4) && (_num3 == 4))
            {
                return 5;
            }
            else if ((_num1 == 3) && (_num2 == 3) && (_num3 == 3))
            {
                return 4;
            }
            else if ((_num1 == 2) && (_num2 == 2) && (_num3 == 2))
            {
                return 3;
            }
            else if ((_num1 == 1) && (_num2 == 1) && (_num3 == 1))
            {
                return 2;
            }
            else
            {
                return 0;
            }
            
        }
        public void checkWin()
        {
            int multiplier = this.multiplier();
            _balance -= _bet;
            _balance += multiplier * _bet;
            _lastWin = multiplier * _bet;
            _spinLimit--;
        }
    }
}
